import { createApp } from "vue";
import App from "./App.vue";
import router from "./routes";
import store from "./store";
import "@/assets/styles.css";
import "@/assets/styles-comp.css";
import "@/assets/styles-list.css";

createApp(App).use(router).use(store).mount("#app");

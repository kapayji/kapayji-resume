import { createRouter, createWebHistory } from "vue-router";

const routerHistory = createWebHistory("/resume");
import MainResume from "@/pages/resume/MainResume";
import HomePage from "@/pages/vue-composition/HomePage";
import MainVueList from "@/pages/vue-list/MainVueList";

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      name: "home",
      component: MainResume,
    },
    {
      path: "/vue-composition",
      name: "vue-comp",
      component: HomePage,
    },
    {
      path: "/vue-list",
      name: "vue-list",
      component: MainVueList,
    },
    {
      path: "/:pathMatch(.*)*",
      redirect: { name: "home" },
    },
  ],
});
export default router;
